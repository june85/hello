package co.kr.helloworld;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by hyunjunroh on 13/02/2017.
 */
@RestController
public class MainController {
    @RequestMapping("/")
    public String index(){
        return "Build now";
    }
}
